# MMLD52

by Bruno Massa

Game created in a weekend for [Ludum Dare 52](https://ldjam.com/events/ludum-dare/52) and [MiniJam 123](https://itch.io/jam/mini-jam-123-web)

a digital version of *Magic Maze*, by *Kasper Lapp*
