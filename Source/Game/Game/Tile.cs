﻿using System;
using System.Collections.Generic;
using FlaxEngine;
using M4554;

namespace MagicMaze;

public sealed class Tile : Script
{
  [Serialize, ShowInEditor] Prefab WallPrefab = default;
  [Serialize, ShowInEditor] Actor Highlight = default;
  [Serialize, ShowInEditor] Actor Explore = default;
  [Serialize, ShowInEditor] Actor Teleport = default;
  [Serialize, ShowInEditor] Actor Timer = default;
  [Serialize, ShowInEditor] Actor Exit = default;
  [Serialize, ShowInEditor] Actor Item = default;
  [Serialize, ShowInEditor] Actor Ladder = default;

  [Header("Data")]
  [ShowInEditor] Model.Tile data = default!;
  [ShowInEditor] bool isHighlighted = false;
  StaticModel staticModel = null;
  Model.GameManager gameManager = default!;
  Model.Pawn pawn = default!;
  List<Model.Direction> wallsCreated = new List<Model.Direction>();
  bool extraWallsCreated = false;
  StaticModel itemModel;

  #region Script
  public override void OnUpdate()
  {
    if (gameManager is null || !isHighlighted)
      return;

    if (InputChecker.GetMouseButtonUp(Actor, MouseButton.Left))
    {
      if (gameManager.Action.Execute(gameManager, pawn, data))
        gameManager.ActionPawnSelect(null, null);
    }
  }

  public override void OnDestroy()
  {
    this.gameManager.Data.OnTileBlockLoaded -= Refresh;
    this.gameManager.OnPawnMovedTile -= OnPawnMovedTile;
  }
  #endregion Script

  #region public
  public void Setup(Model.Tile data, Model.GameManager gameManager)
  {
    this.data = data;
    this.gameManager = gameManager;
    this.gameManager.OnPawnMovedTile += OnPawnMovedTile;
    this.gameManager.Data.OnTileBlockLoaded += Refresh;

    Actor.Name = $"Tile {data.Position}";
    PositionSet(this.data.Position);
    staticModel = Actor.GetChildren<StaticModel>()[0];

    Refresh(null);
  }

  public void SetHighlight(bool isHighlighted, Model.Pawn pawn)
  {
    this.pawn = pawn;
    this.isHighlighted = isHighlighted;
    Highlight.IsActive = isHighlighted;
  }

  #endregion public

  void PositionSet(Int2 position)
  {
    Actor.LocalPosition = new Vector3(position.X, 0, position.Y) * 100;
  }

  void SetTeleport()
  {
    Teleport.IsActive = data.Teleport && !data.IsDeactivated;
    if (Teleport.IsActive)
      Teleport.GetChildren<StaticModel>()[0].SetMaterial(0, (data.Teleport.Instance as Model.PawnType).MaterialInstance);
  }

  void SetExit()
  {
    Exit.IsActive = data.Exit;
    if (Exit.IsActive)
      Exit.GetChildren<StaticModel>()[0].SetMaterial(0, (data.Exit.Instance as Model.PawnType).MaterialInstance);
  }

  void SetTimer()
  {
    Timer.IsActive = data.Timer && !data.IsDeactivated;
  }

  void SetItem()
  {
    Item.IsActive = data.Item && !data.IsDeactivated;
    if (Item.IsActive && itemModel == null)
    {
      Actor.GetChildren<StaticModel>()[0].SetMaterial(0, (data.Item.Instance as Model.PawnType).MaterialInstance);

      var modelprefab = PrefabManager.SpawnPrefab((data.Item.Instance as Model.PawnType).Item, Item);
      itemModel = modelprefab is StaticModel ? modelprefab as StaticModel : modelprefab.GetChildren<StaticModel>()[0];
      itemModel.SetMaterial(0, (data.Item.Instance as Model.PawnType).MaterialInstance);
    }
  }

  void SetExplore()
  {
    if (data.Explore is not null)
    {
      var borders = data.TileBlockBorders(gameManager.Data, true);
      if (borders.Count > 0)
      {
        var rotation = Model.Tile.RotateDegress(borders[0]);
        Explore.LocalOrientation = Quaternion.Euler(0, rotation, 0);

        Explore.IsActive = !data.IsDeactivated && gameManager.Data.TilesBlocks.Count > 0;
        Explore.GetChildren<StaticModel>()[0].SetMaterial(0, (this.data.Explore.Instance as Model.PawnType).MaterialInstance);
      }
    }
  }

  void SetWalls()
  {
    if (!extraWallsCreated && gameManager.Data.TilesBlocks.Count == 0)
    {
      extraWallsCreated = true;
      var borders = data.TileBlockBorders(gameManager.Data, true);
      if (borders.Count > 0)
      {
        var direction = Model.Tile.Int22Direction(borders[0]);
        data.Walls ??= new List<Model.Direction>();
        if (!data.Walls.Contains(direction))
          data.Walls.Add(direction);
      }
    }

    if (data.Walls is not null && data.Walls.Count > wallsCreated.Count)
    {
      foreach (var direction in data.Walls)
      {
        if (!wallsCreated.Contains(direction))
        {
          wallsCreated.Add(direction);
          WallCreate(direction);
        }
      }
    }
  }

  void SetLadder()
  {
    Ladder.IsActive = data.Stairs != Int2.Zero;
    if (Ladder.IsActive)
    {
      var delta = data.LocalPosition - data.Stairs;
      Ladder.Scale = new Vector3(1, 1, delta.Length);
      var angleRad = MathF.Atan2(delta.X, delta.Y);
      Ladder.LocalOrientation = Quaternion.Euler(0, 180 + angleRad * Mathf.RadiansToDegrees, 0);
    }
  }

  void Refresh(Dictionary<Int2, Model.Tile> TilesNew)
  {
    SetWalls();
    SetTimer();
    SetExplore();
    SetTeleport();
    SetItem();
    SetExit();
    SetLadder();
  }

  void WallCreate(Model.Direction direction)
  {
    var wall = (Actor)PrefabManager.SpawnPrefab(WallPrefab, Actor);
    var rotation = Model.Tile.RotateDegress(direction);
    wall.LocalOrientation = Quaternion.Euler(0, rotation, 0);
  }

  void OnPawnMovedTile(Model.Pawn pawn, Model.Tile tile)
  {
    Refresh(null);
  }
}
