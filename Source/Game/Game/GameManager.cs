using System;
using System.Collections.Generic;
using FlaxEngine;

namespace MagicMaze;

public sealed class GameManager : Script
{
  [AssetReference(typeof(Model.Game))]
  [Serialize, ShowInEditor] JsonAsset DataAsset = default;
  [AssetReference(typeof(Model.GameManager))]
  [Serialize, ShowInEditor] JsonAsset GameManagerAsset = default;
  
  Model.Game Data = null;
  Model.GameManager gameManager => GameManagerAsset.Instance as Model.GameManager;
  bool isLoaded = false;

  #region Script
  public override void OnStart()
  {
    Data = DataAsset != null ? ((Model.Game)DataAsset.CreateInstance()) : null;
    if (Data is not null)
    {
      Data.SetupNew();
      gameManager.OnGameLoaded += OnGameLoaded;
      gameManager.Load(Data);
    }
  }

  public override void OnUpdate()
  {
    if (isLoaded && !gameManager.IsGameOver)
    {
      Data.Timer -= Time.DeltaTime;
      if (Data.Timer <= 0)
        gameManager.GameOver(false);
    }
  }
  #endregion Script

  void OnGameLoaded(Model.Game game)
  {
    isLoaded = true;
  }
}