using System.Collections.Generic;
using FlaxEngine;

namespace MagicMaze;

public sealed class CameraDynamics : Script
{
  [Serialize, ShowInEditor] Camera Camera;

  [AssetReference(typeof(Model.GameManager))]
  [Serialize, ShowInEditor] JsonAsset GameManagerAsset = default;

  [Serialize, ShowInEditor] Vector3 tileMultiplier = new Vector3(0, 0, 100);

  [Serialize, ShowInEditor] Vector3 offset = new Vector3(150, 0, 0);
  Model.GameManager gameManager => GameManagerAsset.Instance as Model.GameManager;
  [ShowInEditor] int maxValue = 0;
  [ShowInEditor] int maxint = 0;
  [ShowInEditor] int minint = 0;
  [ShowInEditor] bool update = false;

  public override void OnStart()
  {
    base.OnStart();
    if (gameManager is not null)
      gameManager.OnGameLoaded += OnGameLoaded;
  }

  public override void OnDestroy()
  {
    base.OnDestroy();
    if (gameManager is not null)
    {
      gameManager.Data.OnTileBlockLoaded -= OnTileBlockLoaded;
      gameManager.OnGameLoaded -= OnGameLoaded;
    }
  }
  public override void OnUpdate()
  {
    if (update) Update();
  }

  void OnGameLoaded(Model.Game game)
  {
    gameManager.Data.OnTileBlockLoaded += OnTileBlockLoaded;
    Calculate();
  }

  void OnTileBlockLoaded(Dictionary<Int2, Model.Tile> _)
  {
    Calculate();
  }

  void Calculate()
  {
    maxint = 0;
    minint = 0;
    foreach (var tile in gameManager.Tiles)
    {
      maxint = Mathf.Max(tile.Value.Position.MaxValue, maxint);
      minint = Mathf.Min(tile.Value.Position.MinValue, minint);
    }
    maxValue = Mathf.Max(Mathf.Abs(maxint), Mathf.Abs(minint));
    Update();
  }

  void Update()
  {
    Camera.LocalPosition = maxValue * tileMultiplier + ((maxint + minint) / 2 * offset);
  }
}