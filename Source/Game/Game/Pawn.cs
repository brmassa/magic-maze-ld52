﻿using System;
using System.Collections.Generic;
using FlaxEngine;
using M4554;

namespace MagicMaze;

public sealed class Pawn : Script
{
  [Serialize, ShowInEditor] Actor Item = default!;
  [Serialize, ShowInEditor] Material MaterialSafe = default!;
  [ShowInEditor] Model.Pawn data = default!;
  Model.GameManager gameManager = default!;
  StaticModel model;
  StaticModel modelItem;

  Model.PawnType type => data.Type.Instance as Model.PawnType;

  #region Script
  public override void OnUpdate()
  {
    if (gameManager is null)
      return;

    if (!data.IsSafe && InputChecker.GetMouseButtonUp(Actor, MouseButton.Left))
    {
      var positions = gameManager.Action.Hightlight(data, gameManager.Data);
      gameManager.ActionPawnSelect(data, positions);
    }
  }

  public override void OnDestroy()
  {
    base.OnDestroy();

    if (gameManager is not null)
      this.gameManager.OnPawnMovedTile -= OnPawnMovedTile;
  }
  #endregion Script

  #region public
  public void Setup(Model.Pawn data, Model.GameManager gameManager)
  {
    this.data = data;
    this.gameManager = gameManager;
    if (gameManager is not null)
      this.gameManager.OnPawnMovedTile += OnPawnMovedTile;
    this.data.OnMoved += OnMoved;

    // Actor.Name = $"Pawn {data.Type}";
    PositionSet(this.data.Position);

    var modelprefab = PrefabManager.SpawnPrefab(type.Model, Actor);
    model = modelprefab is StaticModel ? modelprefab as StaticModel : modelprefab.GetChildren<StaticModel>()[0];
    model.SetMaterial(0, type.MaterialInstance);
  }
  #endregion public

  void PositionSet(Int2 position)
  {
    Actor.LocalPosition = new Vector3(position.X, 0, position.Y) * 100;
  }

  void OnMoved(Model.Tile tile)
  {
    PositionSet(tile.Position);
  }

  void OnPawnMovedTile(Model.Pawn pawn, Model.Tile tile)
  {
    Item.IsActive = data.HasItem;
    if (Item.IsActive && modelItem == null)
    {
      var modelprefab = PrefabManager.SpawnPrefab(type.Item, Item);
      modelItem = modelprefab is StaticModel ? modelprefab as StaticModel : modelprefab.GetChildren<StaticModel>()[0];
      modelItem.SetMaterial(0, type.MaterialInstance);
    }
    if (pawn == data && data.IsSafe)
      model.SetMaterial(0, MaterialSafe);
  }
}
