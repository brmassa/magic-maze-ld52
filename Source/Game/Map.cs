using System;
using System.Collections.Generic;
using FlaxEngine;
using FlaxEngine.GUI;

namespace MagicMaze;

public sealed class Map : Script
{
  [AssetReference(typeof(Model.GameManager))]
  [Serialize, ShowInEditor] JsonAsset GameManagerAsset = default;
  [Serialize, ShowInEditor] Prefab TilePrefab = default;
  [Serialize, ShowInEditor] Prefab PawnPrefab = default;
  [ShowInEditor] public Dictionary<Int2, Tile> Tiles = new Dictionary<Int2, Tile>();

  [Header("Show mode")]
  [Serialize, ShowInEditor] bool showMode = false;
  [AssetReference(typeof(Model.Game))]
  [Serialize, ShowInEditor] JsonAsset GamePrefab;

  Model.GameManager GameManager => GameManagerAsset.Instance as Model.GameManager;

  #region Script
  public override void OnStart()
  {
    if (showMode)
    {
      if (GamePrefab is not null)
      {
        var game = (Model.Game)GamePrefab.CreateInstance();
        game.SetupNew();
        OnGameLoaded(game);
      }
      return;
    }
    GameManager.OnGameLoaded += OnGameLoaded;
    GameManager.OnActionPawnSelected += OnActionPawnSelected;
    GameManager.OnActionChanged += OnActionChanged;
  }

  public override void OnDestroy()
  {
    if (showMode)
      return;
    GameManager.OnGameLoaded -= OnGameLoaded;
    GameManager.OnActionPawnSelected -= OnActionPawnSelected;
    GameManager.Data.OnTileBlockLoaded -= LoadTiles;
    GameManager.OnActionChanged -= OnActionChanged;
  }
  #endregion Script

  void OnGameLoaded(Model.Game game)
  {
    if (showMode)
      return;
    GameManager.Data.OnTileBlockLoaded += LoadTiles;

    foreach (var pawnData in game.Pawns)
    {
      var pawn = PrefabManager.SpawnPrefab(PawnPrefab, Actor).GetScript<Pawn>();

      pawn.Setup(pawnData, GameManager);
    }

    LoadTiles(game.Tiles);
  }

  void OnActionChanged(Model.IAction _)
  {
    foreach (KeyValuePair<Int2, Tile> tileData in Tiles)
      tileData.Value.SetHighlight(false, null);
  }

  void OnActionPawnSelected(Model.Pawn pawn, List<Int2> positions)
  {
    foreach (KeyValuePair<Int2, Tile> tileData in Tiles)
      tileData.Value.SetHighlight(false, null);

    if (positions is null)
      return;
    foreach (var position in positions)
    {
      Tile tile;
      Tiles.TryGetValue(position, out tile);
      if (tile is not null)
        tile.SetHighlight(true, pawn);
    }
  }

  void LoadTiles(Dictionary<Int2, Model.Tile> TilesNew)
  {
    foreach (KeyValuePair<Int2, Model.Tile> tileData in TilesNew)
    {
      var tile = PrefabManager.SpawnPrefab(TilePrefab, Actor).GetScript<Tile>();
      tile.Setup(tileData.Value, showMode ? null : GameManager);
      Tiles.Add(tileData.Key, tile);
    }
  }
}