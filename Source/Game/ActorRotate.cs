using System;
using System.Collections.Generic;
using FlaxEngine;
using FlaxEngine.GUI;

namespace MagicMaze;

public sealed class ActorRotate : Script
{
  [Serialize, ShowInEditor] Vector3 Speed = default!;

  #region Script

  public override void OnUpdate()
  {
    Actor.LocalEulerAngles = Actor.LocalEulerAngles + Speed * Time.DeltaTime;
  }
  #endregion Script
}