using System;
using System.Collections.Generic;
using FlaxEngine;
using FlaxEngine.GUI;

namespace MagicMaze;

public sealed class MainMenu : Script
{
  [AssetReference(typeof(Model.GameManager))]
  [Serialize, ShowInEditor] JsonAsset GameManagerAsset = default;
  [Serialize, ShowInEditor] UIControl TutorialButton = default;
  [Serialize, ShowInEditor] UIControl NewGameButton = default;
  [Serialize, ShowInEditor] UIControl QuitButton = default;

  [Serialize, ShowInEditor] SceneReference GameScene;

  // Label actionsLabel => ActionsLabel.Get<Label>();
  // Image actionIcon => ActionIcon.Get<Image>();
  // Label actionLabel => ActionLabel.Get<Label>();

  Model.GameManager GameManager => GameManagerAsset.Instance as Model.GameManager;

  #region Script
  public override void OnStart()
  {
    TutorialButton.Get<Button>().Clicked += TutorialToggle;
    NewGameButton.Get<Button>().Clicked += GameNew;
    QuitButton.Get<Button>().Clicked += Quit;
  }

  public override void OnDestroy()
  {
  }
  #endregion Script
  void GameNew()
  {
    Level.ChangeSceneAsync(GameScene);
  }

  void TutorialToggle()
  {
  }


  void Quit()
  {
    Engine.RequestExit();
  }
}

