using System;
using System.Collections.Generic;
using FlaxEngine;
using FlaxEngine.GUI;

namespace MagicMaze;

public sealed class ActionPanel : Script
{
  [Serialize, ShowInEditor] bool showMode = false;
  [AssetReference(typeof(Model.GameManager))]
  [Serialize, ShowInEditor] JsonAsset GameManagerAsset = default;
  [Serialize, ShowInEditor] UIControl ActionsLabel = default;
  [Serialize, ShowInEditor] UIControl NextButton = default;
  [Serialize, ShowInEditor] UIControl ActionIcon = default;
  [Serialize, ShowInEditor] UIControl ActionLabel = default;

  Label actionsLabel => ActionsLabel.Get<Label>();
  Image actionIcon => ActionIcon.Get<Image>();
  Label actionLabel => ActionLabel.Get<Label>();

  Model.GameManager GameManager => GameManagerAsset.Instance as Model.GameManager;

  #region Script
  public override void OnStart()
  {
    NextButton.Get<Button>().Clicked += NextAction;
    if (showMode)
      return;
    GameManager.OnActionChanged += OnActionChanged;
    GameManager.OnGameLoaded += OnGameLoaded;
  }

  public override void OnDestroy()
  {
    if (showMode)
      return;
    GameManager.OnActionChanged -= OnActionChanged;
    GameManager.OnGameLoaded -= OnGameLoaded;
  }
  #endregion Script

  #region public 

  public void NextAction()
  {
    if (showMode)
      return;
    if (!GameManager.IsGameOver)
      GameManager.ActionNext();
  }
  #endregion public

  void OnActionChanged(Model.IAction action)
  {
    Refresh();
  }

  void OnGameLoaded(Model.Game game)
  {
    Refresh();
  }

  void Refresh()
  {
    actionsLabel.Text = $"{GameManager.ActionCurrent + 1} / {GameManager.ActionQty}";
    actionLabel.Text = GameManager.Action.Title;
    (actionIcon.Brush as TextureBrush).Texture = GameManager.Action.Texture;
  }
}