using FlaxEngine;

namespace M4554;
static class InputChecker
{
  static public bool GetMouseButtonUp(Actor actor, MouseButton MouseButton)
  {
    if (Input.GetMouseButtonUp(MouseButton) && HasCollided(actor))
      return true;
    return false;
  }

  static public bool HasCollided(Actor actor)
  {
    var mousePosition = Input.MousePosition;
    var ray = Camera.MainCamera.ConvertMouseToRay(mousePosition);
    if (Physics.RayCast(ray.Position, ray.Direction, out var hit))
    {
      return HasCollidedActor(actor, hit);
    }
    return false;
  }

  static public bool HasCollidedActor(Actor actor, RayCastHit hit)
  {
    if (actor is Collider && actor == hit.Collider)
    {
      return true;
    }
    else
    {
      foreach (var child in actor.GetChildren<Actor>())
      {
        if (HasCollidedActor(child, hit))
          return true;
      }
    }
    return false;
  }
}