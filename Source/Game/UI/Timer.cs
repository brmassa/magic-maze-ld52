using System;
using System.Collections.Generic;
using FlaxEngine;
using FlaxEngine.GUI;

namespace MagicMaze;

public sealed class Timer : Script
{
  [AssetReference(typeof(Model.GameManager))]
  [Serialize, ShowInEditor] JsonAsset GameManagerAsset = default;
  [Serialize, ShowInEditor] UIControl TimerLabel = default;
  [Serialize, ShowInEditor] int[] FontSize = new int[2] { 9, 15 };
  Model.GameManager GameManager => GameManagerAsset.Instance as Model.GameManager;
  Label timerLabel => TimerLabel.Get<Label>();
  bool isLoaded = false;

  #region Script
  public override void OnStart()
  {
    GameManager.OnGameLoaded += OnGameLoaded;
  }

  public override void OnUpdate()
  {
    if (isLoaded)
      Refresh();
  }
  #endregion Script

  void OnGameLoaded(Model.Game game)
  {
    isLoaded = true;
  }

  void Refresh()
  {
    timerLabel.Font.Size = Int2Lerp(FontSize[0], FontSize[1], 1 - GameManager.Timer.TotalSeconds / (3 * 60));
    var label = GameManager.Timer.TotalSeconds > 60 ? "mm\\:ss" : "mm\\:ss\\.ff";
    timerLabel.Text = GameManager.Timer.ToString(label);
  }

  int Int2Lerp(int min, int max, double amount)
  {
    return (int)(min + (max - min) * amount);
  }
}