using System;
using System.Collections.Generic;
using FlaxEngine;
using FlaxEngine.GUI;

namespace MagicMaze;

public sealed class SceneChanger : Script
{
  [Serialize, ShowInEditor] UIControl Button = default;
  [Serialize, ShowInEditor] SceneReference SceneNew;

  #region Script
  public override void OnStart()
  {
    Button.Get<Button>().Clicked += SceneChange;
  }
  #endregion Script
  void SceneChange()
  {
    Level.ChangeSceneAsync(SceneNew);
  }
}

