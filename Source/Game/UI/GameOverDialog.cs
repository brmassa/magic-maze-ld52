using FlaxEngine;
using FlaxEngine.GUI;

namespace MagicMaze;
sealed class GameOverDialog : Script
{
  [Serialize, ShowInEditor] UIControl GameOverPanel = default;
  [Serialize, ShowInEditor] UIControl GameWinPanel = default;
  [AssetReference(typeof(Model.GameManager))]
  [Serialize, ShowInEditor] JsonAsset GameManagerAsset = default;
  Model.GameManager GameManager => GameManagerAsset.Instance as Model.GameManager;

  public override void OnStart()
  {
    GameManager.OnGameOver += OnGameOver;
  }

  public override void OnDestroy()
  {
    GameManager.OnGameOver -= OnGameOver;
  }

  void OnGameOver(bool win)
  {
    GameOverPanel.IsActive = !win;
    GameWinPanel.IsActive = win;
  }
}