using System;
using System.Collections.Generic;
using FlaxEngine;

namespace MagicMaze.Model;

[ContentContextMenu("MagicMaze/GameManager")]
public sealed class GameManager : IGame
{
  [ShowInEditor, ExpandGroups] public Model.Game Data = null;

  #region public
  [HideInEditor] public Action<Model.Game> OnGameLoaded { get; set; }
  [HideInEditor] public Action<Model.IAction> OnActionChanged { get; set; }
  [HideInEditor] public Action<bool> OnGameOver { get; set; }
  [HideInEditor] public Action<Pawn, List<Int2>> OnActionPawnSelected { get; set; }
  [HideInEditor] public Action<Pawn, Tile> OnPawnMovedTile { get; set; }
  [HideInEditor] public Model.IAction Action => Data.Actions[ActionCurrent].Instance as Model.IAction;
  public int ActionCurrent => Data.ActionCurrent;
  public int ActionQty => Data.Actions.Count;
  public TimeSpan Timer => TimeSpan.FromSeconds(Data.Timer);
  public bool IsGameOver => Data.IsGameOver;
  public Dictionary<Int2, Tile> Tiles => Data.Tiles;

  public void Load(Model.Game Data)
  {
    this.Data = Data;
    OnGameLoaded?.Invoke(Data);
  }

  public void ActionNext()
  {
    Data.ActionCurrent = ++Data.ActionCurrent % Data.Actions.Count;
    OnActionChanged?.Invoke(Action);
  }

  public void GameOver(bool win)
  {
    Data.IsGameOver = true;
    OnGameOver?.Invoke(win);
  }

  public void ActionPawnSelect(Pawn pawn, List<Int2> tiles)
  {
    OnActionPawnSelected?.Invoke(pawn, tiles);
  }

  public void PawnMoveTile(Pawn pawn, Tile tile)
  {
    if (tile.Timer)
      TimerFlip(tile);

    if (tile.Exit && pawn.HasItem && pawn.Type == tile.Exit)
    {
      pawn.IsSafe = true;
    }
    var allPawnsSafe = true;
    foreach (var pawnOther in Data.Pawns)
      allPawnsSafe &= pawnOther.IsSafe;

    if (tile.Item && pawn.Type == tile.Item)
    {
      var allPawnsSet = true;
      foreach (var pawnOther in Data.Pawns)
      {
        Tile tileOther;
        Data.Tiles.TryGetValue(pawnOther.Position, out tileOther);
        allPawnsSet &= (tileOther is not null) && tileOther.Item == pawnOther.Type;
      }

      if (allPawnsSet)
      {
        foreach (var pawnOther in Data.Pawns)
        {
          pawnOther.HasItem = true;
          Tile tileOther;
          Data.Tiles.TryGetValue(pawnOther.Position, out tileOther);
          tileOther.Item = null;
          tileOther.IsDeactivated = true;
        }

        foreach (var tileOther in Data.Tiles)
        {
          if (tileOther.Value.Teleport)
          {
            tileOther.Value.Teleport = null;
            tileOther.Value.IsDeactivated = true;
          }
        }
      }
    }

    OnPawnMovedTile?.Invoke(pawn, tile);

    if (allPawnsSafe)
      GameOver(true);
    return;
  }
  #endregion public

  void TimerFlip(Tile tile)
  {
    Data.Timer = (3 * 60) - Data.Timer;
    tile.Timer = false;
    tile.IsDeactivated = true;

    List.Shuffle(Data.Actions);
    Data.ActionCurrent = Data.Actions.Count - 1;
    ActionNext();
  }
}