using System;
using System.Collections.Generic;
using FlaxEngine;

namespace MagicMaze.Model;

[ContentContextMenu("MagicMaze/Game")]
public sealed class Game : IGame
{
  [AssetReference(typeof(Action))]
  [Serialize, ShowInEditor] public List<JsonAsset> Actions { get; set; } = default;
  [Serialize, ShowInEditor] public int ActionCurrent { get; set; } = default;
  [Serialize, ShowInEditor] public bool ActionUsed { get; set; } = false;
  [Serialize, ShowInEditor] public float Timer { get; set; } = 3f * 60;
  [Serialize, ShowInEditor] public List<Pawn> Pawns { get; set; } = default;
  [Serialize, ShowInEditor] public Dictionary<Int2, Tile> Tiles { get; set; } = new Dictionary<FlaxEngine.Int2, Tile>();
  [AssetReference(typeof(TileBlock))]
  [Serialize, ShowInEditor] public List<JsonAsset> TilesBlocks { get; set; } = default;
  [Serialize, ShowInEditor] public bool IsGameOver { get; set; } = false;
  [Serialize, ShowInEditor] bool randomizePawns = true;
  [Serialize, ShowInEditor] bool randomizeActions = true;

  [HideInEditor] public Action<Dictionary<Int2, Tile>> OnTileBlockLoaded { get; set; }

  public void SetupNew()
  {
    if (Tiles.Count == 0)
    {
      TileBlockUse(new Int2(0, 0), Direction.N, true);

      // Shuffle action order
      if (randomizeActions)
        List.Shuffle(Actions);

      // Shuffle pawns' inital positions
      if (randomizePawns)
      {
        List<Int2> pawnPositions = new List<FlaxEngine.Int2>()
    {
      new Int2(1,1),
      new Int2(2,1),
      new Int2(1,2),
      new Int2(2,2)
    };
        List.Shuffle(pawnPositions);
        for (int i = 0; i < pawnPositions.Count; i++)
          Pawns[i].Position = pawnPositions[i];
      }
    }
  }

  public void TileBlockUse(Int2 positionBlockOrigin, Direction direction, bool skip = false)
  {
    var TilesNew = new Dictionary<Int2, Tile>();
    var block = TilesBlocks[0].CreateInstance() as TileBlock;
    TilesBlocks.RemoveAt(0);
    var tiles = block.TileBlockSetup(positionBlockOrigin, direction, skip);
    foreach (var tile in tiles)
    {
      Tiles.Add(tile.Position, tile);
      TilesNew.Add(tile.Position, tile);
    }

    OnTileBlockLoaded?.Invoke(TilesNew);
  }
}