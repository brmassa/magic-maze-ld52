using System;
using System.Collections.Generic;
using FlaxEngine;

public sealed class List
{
  public static void Shuffle<T>(List<T> list)
  {
    int n = list.Count;
    var r = new Random();
    while (--n > 1)
    {
      int k = r.Next(0, list.Count);
      T value = list[k];
      list[k] = list[n];
      list[n] = value;
    }
  }
}
