using System;
using System.Collections.Generic;
using FlaxEngine;

namespace MagicMaze.Model;

public sealed class Pawn
{
  [Serialize, ShowInEditor] public Int2 Position { get; set; } = default;
  [AssetReference(typeof(PawnType))]
  [Serialize, ShowInEditor] public JsonAsset Type { get; set; }
  [Serialize, ShowInEditor] public bool HasItem { get; set; }
  [Serialize, ShowInEditor] public bool IsSafe { get; set; }

  public Action<Tile> OnMoved { get; set; }

  public void Move(Tile tile)
  {
    Position = tile.Position;
    OnMoved?.Invoke(tile);
  }
}