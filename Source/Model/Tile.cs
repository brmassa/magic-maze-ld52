using System.Collections.Generic;
using FlaxEngine;

namespace MagicMaze.Model;

public sealed class Tile
{
  [Serialize, ShowInEditor] public Int2 LocalPosition { get; set; } = default!;
  [Serialize, ShowInEditor] public Int2 Position { get; set; } = default!;
  [Serialize, ShowInEditor] public Int2 Stairs { get; set; } = Int2.Zero;
  [AssetReference(typeof(PawnType))]
  [Serialize, ShowInEditor] public JsonAsset Teleport { get; set; }
  [AssetReference(typeof(PawnType))]
  [Serialize, ShowInEditor] public JsonAsset Explore { get; set; }
  [AssetReference(typeof(PawnType))]
  [Serialize, ShowInEditor] public JsonAsset Item { get; set; }
  [AssetReference(typeof(PawnType))]
  [Serialize, ShowInEditor] public JsonAsset Exit { get; set; }
  [Serialize, ShowInEditor] public bool Timer = false;
  [Serialize, ShowInEditor] public bool IsDeactivated { get; set; }
  [Serialize, ShowInEditor, ExpandGroups] public List<Direction> Walls { get; set; } = default!;

  public void Setup(Direction direction, Int2 max)
  {
    for (int i = 0; i < (int)direction; i++)
    {
      LocalPosition = Int2Extra.Transpose(LocalPosition, max);
      if (Stairs != Int2.Zero)
        Stairs = Int2Extra.Transpose(Stairs, max);

      if (Walls is not null)
        for (int w = 0; w < Walls.Count; w++)
          Walls[w] = Model.Tile.Int22Direction(Int2Extra.Transpose(Walls[w], Int2.Zero));
    }
  }

  public static int RotateDegress(Int2 direction)
  {
    if (direction == Int2.UnitY)
      return 0;
    else if (direction == Int2.UnitX)
      return 90;
    else if (direction == -Int2.UnitY)
      return 180;
    // else if (directionDelta == -Int2.UnitY)
    // return 270;

    return 270;
  }

  public static int RotateDegress(Direction direction)
  {
    if (direction == Direction.N)
      return 0;
    else if (direction == Direction.E)
      return 90;
    else if (direction == Direction.S)
      return 180;
    // else if (direction == Direction.N)
    // return 270;

    return 270;
  }

  public static Int2 Direction2Int2(Direction direction)
  {
    if (direction == Direction.N)
      return Int2.UnitY;
    else if (direction == Direction.E)
      return Int2.UnitX;
    else if (direction == Direction.S)
      return -Int2.UnitY;
    // else if (direction == Direction.W)
    // return -Int2.UnitX;

    return -Int2.UnitX;
  }

  public static Direction Int22Direction(Int2 direction)
  {
    if (direction == Int2.UnitY)
      return Direction.N;
    else if (direction == Int2.UnitX)
      return Direction.E;
    else if (direction == -Int2.UnitY)
      return Direction.S;
    // else if (directionDelta == -Int2.UnitX)
    // return Direction.W;

    return Direction.W;
  }

  public List<Int2> TileBlockBorders(Game game, bool testIfExists = false)
  {
    var borders = new List<Int2>();
    if (LocalPosition.X == 0)
      BorderTestIfExists(game, borders, Position, new Int2(-1, 0), testIfExists);
    if (LocalPosition.X == 3)
      BorderTestIfExists(game, borders, Position, new Int2(1, 0), testIfExists);
    if (LocalPosition.Y == 0)
      BorderTestIfExists(game, borders, Position, new Int2(0, -1), testIfExists);
    if (LocalPosition.Y == 3)
      BorderTestIfExists(game, borders, Position, new Int2(0, 1), testIfExists);
    return borders;
  }

  public void BorderTestIfExists(Game game, List<Int2> borders, Int2 positionCurrent, Int2 positionExtra, bool testIfExists = false)
  {
    if (!testIfExists)
    {
      borders.Add(positionExtra);
    }
    else
    {
      if (Walls is not null && Walls.Contains(Tile.Int22Direction(positionExtra)))
        return;
      Tile? tile;
      game.Tiles.TryGetValue(positionCurrent + positionExtra, out tile);
      if (tile is null)
        borders.Add(positionExtra);
    }
  }
}
