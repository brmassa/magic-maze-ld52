using System.Collections.Generic;
using FlaxEngine;

namespace MagicMaze.Model;

[ContentContextMenu("MagicMaze/PawnType")]
public sealed class PawnType
{
  [Serialize, ShowInEditor] public Color Color { get; set; } = default;
  [Serialize, ShowInEditor] public MaterialInstance MaterialInstance { get; set; } = default;
  [Serialize, ShowInEditor] public Prefab Model { get; set; } = default;
  [Serialize, ShowInEditor] public Prefab Item { get; set; } = default;
}