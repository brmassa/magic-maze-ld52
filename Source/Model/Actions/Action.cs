using System.Collections.Generic;
using FlaxEngine;
using FlaxEngine.GUI;

namespace MagicMaze.Model;
public abstract class Action : IAction
{
  public string Title { get; set; }
  public Texture Texture { get; set; }

  abstract public List<Int2> Hightlight(Pawn pawn, Game game);
  abstract public bool Execute(GameManager gameManager, Pawn pawn, Tile tile);
}