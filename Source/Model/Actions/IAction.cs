using System.Collections.Generic;
using FlaxEngine;

namespace MagicMaze.Model;

public interface IAction
{
  string Title { get; set; }
  Texture Texture { get; set; }
  List<Int2> Hightlight(Pawn pawn, Game game);
  bool Execute(GameManager gameManager, Pawn pawn, Tile tile);
}