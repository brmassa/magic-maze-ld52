using System.Collections.Generic;
using FlaxEngine;

namespace MagicMaze.Model;

[ContentContextMenu("MagicMaze/ActionTeleport")]
class ActionTeleport : Action
{
  [Serialize, ShowInEditor] public Int2 Direction { get; set; } = Int2.One;
  [Serialize, ShowInEditor] public int Qty { get; set; } = 50;
  public override List<Int2> Hightlight(Pawn pawn, Game game)
  {
    var positions = new List<Int2>();

    foreach (var tile in game.Tiles)
    {
      if (tile.Value.Teleport == pawn.Type && tile.Value.Position != pawn.Position)
        positions.Add(tile.Value.Position);
    }

    return positions;
  }
  public override bool Execute(GameManager gameManager, Pawn pawn, Tile tile)
  {
    if (tile.IsDeactivated)
      return false;

    pawn.Move(tile);
    gameManager.PawnMoveTile(pawn, tile);
    return true;
  }
}