using System.Collections.Generic;
using FlaxEngine;

namespace MagicMaze.Model;

[ContentContextMenu("MagicMaze/ActionLadder")]
class ActionLadder : Action
{
  public override List<Int2> Hightlight(Pawn pawn, Game game)
  {
    var positions = new List<Int2>();

    foreach (var tile in game.Tiles)
    {
      if (tile.Value.Position == pawn.Position && tile.Value.Stairs != Int2.Zero)
        positions.Add(tile.Value.Position);
    }

    return positions;
  }
  public override bool Execute(GameManager gameManager, Pawn pawn, Tile tile)
  {
    var tileOtherPosition = tile.Stairs + tile.Position - tile.LocalPosition;
    Tile tileOther;
    gameManager.Tiles.TryGetValue(tileOtherPosition, out tileOther);
    

    if (tileOther is null)
      return false;

    pawn.Move(tileOther);
    gameManager.PawnMoveTile(pawn, tileOther);
    return true;
  }
}