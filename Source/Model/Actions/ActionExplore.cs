using System.Collections.Generic;
using FlaxEngine;

namespace MagicMaze.Model;

[ContentContextMenu("MagicMaze/ActionExplore")]
class ActionExplore : Action
{
  [Serialize, ShowInEditor] public Int2 Direction { get; set; } = Int2.One;
  public override List<Int2> Hightlight(Pawn pawn, Game game)
  {
    var positions = new List<Int2>();

    foreach (var tile in game.Tiles)
    {
      if (tile.Value.Position == pawn.Position && tile.Value.Explore == pawn.Type)
        positions.Add(tile.Value.Position);
    }

    return positions;
  }
  public override bool Execute(GameManager gameManager, Pawn pawn, Tile tile)
  {
    if (tile.IsDeactivated || gameManager.Data.TilesBlocks.Count == 0)
      return false;

    var borders = tile.TileBlockBorders(gameManager.Data, true);
    if (borders.Count > 0)
    {
      var direction = Tile.Int22Direction(borders[0]);
      // tile.Explore = null;
      tile.IsDeactivated = true;
      gameManager.Data.TileBlockUse(tile.Position - tile.LocalPosition, direction);
    }
    return true;
  }
}