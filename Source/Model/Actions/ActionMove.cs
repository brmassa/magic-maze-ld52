using System.Collections.Generic;
using FlaxEngine;

namespace MagicMaze.Model;

[ContentContextMenu("MagicMaze/ActionMove")]
class ActionMove : Action
{
  [Serialize, ShowInEditor] public Int2 Direction { get; set; } = Int2.One;
  public override List<Int2> Hightlight(Pawn pawn, Game game)
  {
    var positions = new List<Int2>();

    var position = pawn.Position;
    Tile? tile;
    game.Tiles.TryGetValue(position, out tile);
    var testNext = isTileValid(game, tile, 1);

    var i = 0;
    while (testNext)
    {
      position = pawn.Position + (Direction * ++i);
      game.Tiles.TryGetValue(position, out tile);
      testNext = isTileValid(game, tile, 1);
      if (isTileValid(game, tile, -1))
        positions.Add(position);
      else
        testNext = false;
    }
    return positions;
  }

  bool isTileValid(Game game, Tile tile, int forward)
  {
    if (tile is null)
      return false;

    // Check if there is a wall
    var hasWall = false;
    if (tile.Walls is not null)
    {
      foreach (var wall in tile.Walls)
        hasWall |= (Tile.Direction2Int2(wall) == Direction * (Int2.One * forward));
    }

    // Check if a pawn is placed here (consider as a previous wall)
    if (forward == -1)
    {
      foreach (var pawn in game.Pawns)
        hasWall |= pawn.Position == tile.Position;
    }
    return !hasWall;
  }

  public override bool Execute(GameManager gameManager, Pawn pawn, Tile tile)
  {
    pawn.Move(tile);
    gameManager.PawnMoveTile(pawn, tile);
    return true;
  }
}