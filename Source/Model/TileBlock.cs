using System.Collections.Generic;
using FlaxEngine;
#if FLAX_EDITOR
using System.Linq;
#endif

namespace MagicMaze.Model;

[ContentContextMenu("MagicMaze/TileBlock")]
public sealed class TileBlock
{
  [Serialize, ShowInEditor] public List<Tile> Tiles { get; set; } = default!;
  [Serialize, ShowInEditor] public Int2 Orientation { get; set; } = Int2.UnitX;

  public Int2 Max => Int2.One * 3;
  public Int2 Entry => new Int2(1, 4);

  public List<Tile> TileBlockSetup(Int2 positionBlock, Direction direction, bool skip = false)
  {
    var list = new List<Tile>();
    var offset = skip ? Int2.Zero : Entry;
    for (int i = 0; i < (int)direction; i++)
      offset = Int2Extra.Transpose(offset, Int2.Zero);
    foreach (var tile in Tiles)
    {
      tile.Setup(direction, Max);
      tile.Position = (tile.LocalPosition + positionBlock + offset);
      list.Add(tile);
    }
    return list;
  }
#if FLAX_EDITOR
  public void WallClear()
  {
    foreach (var tile in Tiles)
    {
      tile.Walls = null;
    }
  }
  public void WallGenerator()
  {
    var neighbourPositions = new Int2[] { Int2.UnitX, -Int2.UnitX, Int2.UnitY, -Int2.UnitY };
    var positionInitial = new Int2(1, 0);
    foreach (var tile in Tiles)
    {
      foreach (var neighbourPosition in neighbourPositions)
      {
        var position = tile.LocalPosition + neighbourPosition;
        if (!HasNeighbour(position))
        {
          if (!IsOutideBlock(position)
          || (IsOutideBlock(position) && tile.Explore is null && (tile.LocalPosition != positionInitial)))
          {
            tile.Walls ??= new List<Direction>();
            tile.Walls.Add(Tile.Int22Direction(neighbourPosition));
          }
        }
      }

      if (tile.Walls is not null)
        tile.Walls = tile.Walls.Distinct().ToList();
    }
  }

  bool HasNeighbour(Int2 position)
  {
    foreach (var tile in Tiles)
      if (tile.LocalPosition == position)
        return true;
    return false;
  }

  bool IsOutideBlock(Int2 position)
  {
    return (position.X > Max.X || position.Y > Max.Y || position.X < 0 || position.Y < 0);
  }
#endif
}