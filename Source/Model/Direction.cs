namespace MagicMaze.Model;

public enum Direction
{
  N,
  E,
  S,
  W
}