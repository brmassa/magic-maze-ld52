using FlaxEngine;

namespace MagicMaze;

public static class Int2Extra
{
  public static Int2 Transpose(Int2 position, Int2 max)
  {
    return new Int2(position.Y, -position.X + max.X);
  }

  public static Int2 Transpose(Model.Direction direction, Int2 max)
  {
    var position = Model.Tile.Direction2Int2(direction);
    return new Int2(position.Y, -position.X + max.X);
  }
}