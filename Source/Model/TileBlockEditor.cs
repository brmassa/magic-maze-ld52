#if FLAX_EDITOR
using FlaxEditor.CustomEditors;
using FlaxEditor.CustomEditors.Editors;
using FlaxEngine;

namespace MagicMaze.Model;

  [CustomEditor(typeof(TileBlock))]
  public sealed class TileBlockditor : GenericEditor
  {
    FlaxEditor.CustomEditors.Elements.ButtonElement button;
    FlaxEditor.CustomEditors.Elements.ButtonElement buttonClear;

    public override void Initialize(LayoutElementsContainer layout)
    {
      var values = (Values[0] as TileBlock);

      button = layout.Button("Generate Walls", Color.Green);
      button.Button.Clicked += () => values.WallGenerator();
      buttonClear = layout.Button("Clear Walls", Color.Red);
      buttonClear.Button.Clicked += () => values.WallClear();

      base.Initialize(layout);
    }
  }
#endif
